import unittest


class CallsCounter(object):

    def __init__(self):
        self.calls = {}

    def register(self, func):
        def wrapper(*args, **kwargs):
            self.calls[func.__name__] = self.calls.get(func.__name__, 0) + 1
            return func(*args, **kwargs)
        return wrapper

    def get_calls(self):
        return sorted(self.calls.items(), key=lambda x: -x[1])

    @property
    def min(self):
        if not self.calls:
            return None, None
        return min(self.calls.items(), key=lambda x: x[1])

    @property
    def max(self):
        if not self.calls:
            return None, None
        return max(self.calls.items(), key=lambda x: x[1])


class TestCallsCounter(unittest.TestCase):
    def test(self):
        counter = CallsCounter()

        @counter.register
        def f1():
            pass

        @counter.register
        def f2():
            pass

        @counter.register
        def f3():
            pass

        self.assertEqual(counter.min, (None, None))
        self.assertEqual(counter.max, (None, None))

        for _ in xrange(10):
            f2()

        for _ in xrange(4):
            f3()

        for _ in xrange(7):
            f1()

        self.assertEqual(counter.get_calls(), [
            ('f2', 10),
            ('f1', 7),
            ('f3', 4),
        ])

        self.assertEqual(counter.max, ('f2', 10))
        self.assertEqual(counter.min, ('f3', 4))


if __name__ == '__main__':
    unittest.main(verbosity=2)
